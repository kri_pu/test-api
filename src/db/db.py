from beanie import init_beanie
from motor.motor_asyncio import AsyncIOMotorClient

async def init_db(database_url, database_name, document_models):
    client = AsyncIOMotorClient(database_url)
    await init_beanie(database=client[database_name], document_models=document_models)
