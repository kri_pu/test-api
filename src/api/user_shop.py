from beanie import DeleteRules, WriteRules
from schemas.user_product import *
from fastapi import APIRouter
from models.models import *

users_router = APIRouter(
    prefix='/user',
    tags=['User']
)

@users_router.post("/create/", response_model=UserOut)
async def create_user(user: UserIn):
    user_model = UserModel(**user.model_dump())
    item = await user_model.insert(link_rule=WriteRules.WRITE)
    await item.fetch_all_links()
    return item

@users_router.get("/view_all/", response_model=UserOut)
async def get_users():
    users = UserModel.find_all()
    return users

@users_router.get("/find/{user_id}/", response_model=UserOut)
async def get_user(user_id: str):
    user = await UserModel.get(user_id)
    return user

@users_router.put("/update/{user_id}", response_model=UserOut)
async def update_user(user_id: str, user: UserIn):
    user_new = await UserModel.get(user_id)
    item = await user_new.set(
        {UserModel.name: user.name,
         UserModel.surname: user.surname,
         UserModel.age: user.age,
         UserModel.address: user.address,
         UserModel.passport: user.passport,
         UserModel.orders: user.orders,
         }
    )
    item = await user_new.save(link_rule=WriteRules.WRITE)
    return item

@users_router.delete("/delete/{user_id}")
async def delete_user(user_id: str):
    user = await UserModel.get(user_id)
    await user.delete(link_rule=DeleteRules.DELETE_LINKS)
    return f'{user_id} удалён'



