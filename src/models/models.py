from typing import List, Optional
from beanie import Document, Link

class ProductModel(Document):
    product_name: str

class UserModel(Document): 
    name: str
    surname: str
    age: int
    address: str | None
    passport: str | None
    orders: Optional[List[Link[ProductModel]]]


