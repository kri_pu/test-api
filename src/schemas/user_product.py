from typing import List, Optional
from beanie import Link
from pydantic import BaseModel
from models.models import *
from beanie import PydanticObjectId

class Product(BaseModel):
    product_name: str

class ProductOut(Product):
    id: PydanticObjectId

class ProductIn(Product):
    pass

class User(BaseModel): 
    name: str
    surname: str
    age: int
    address: str | None
    passport: str
    orders: Optional[List[Link[ProductModel]]]

class UserOut(User):
    id: PydanticObjectId
    

class UserIn(User):
    orders: List[str]

