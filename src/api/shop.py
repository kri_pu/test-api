from beanie import WriteRules
from schemas.user_product import *
from fastapi import APIRouter, HTTPException
from models.models import *

product_router = APIRouter(
    prefix='/product',
    tags=['Product']
)

@product_router.post("/create/", response_model=ProductOut)
async def create_product(product: ProductIn):
    prod_model = ProductModel(**product.model_dump())
    item = await prod_model.insert(link_rule=WriteRules.WRITE)
    await item.fetch_all_links()
    return item

@product_router.get("/view_all", response_model=ProductOut)
async def get_all_products():
    products = ProductModel.find_all()
    if not products:
        raise HTTPException(status_code=404, detail="Список продуктов пуст")
    return products

@product_router.get("/find/{product_id}/", response_model=ProductOut)
async def get_product(product_id: str):
    product = await ProductModel.get(product_id)
    return product

@product_router.put("/update/{product_id}", response_model=ProductOut)
async def update_product(product_id: str, product: ProductIn):
    prod = await ProductModel.get(product_id)
    prod.product_name = product.product_name
    item = await prod.save()
    return item

@product_router.delete("/delete/{product_id}")
async def delete_product(product_id: str):
    prod = await ProductModel.get(product_id)
    await prod.delete()
    return f'{product_id} удалён'

