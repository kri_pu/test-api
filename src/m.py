import uvicorn
from models.models import ProductModel, UserModel
from fastapi import FastAPI
from api.shop import product_router
from api.user_shop import users_router
from db.db import init_db

app = FastAPI(
    title="Users, product"
)

@app.on_event("startup")
async def on_startup():
    await init_db("mongodb://localhost:27017", "user_product", [UserModel, ProductModel])


app.include_router(product_router)
app.include_router(users_router)

if __name__ == "__main__":
    uvicorn.run("m:app", host='127.0.0.1', port=8000, reload=True)
